#-------------------------------------------------
#
# Project created by QtCreator 2022-08-22T16:21:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WingHexDisasm
TEMPLATE = lib
CONFIG += plugin

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    winghexdisasm.cpp

HEADERS += \
    capstone/arm.h \
    capstone/arm64.h \
    capstone/bpf.h \
    capstone/capstone.h \
    capstone/evm.h \
    capstone/m680x.h \
    capstone/m68k.h \
    capstone/mips.h \
    capstone/mos65xx.h \
    capstone/platform.h \
    capstone/ppc.h \
    capstone/riscv.h \
    capstone/sparc.h \
    capstone/systemz.h \
    capstone/tms320c64x.h \
    capstone/wasm.h \
    capstone/x86.h \
    capstone/xcore.h \
    winghexdisasm.h \
    ../WingHexExplorer/WingHexExplorer/plugin/iwingplugin.h

DISTFILES += WingHexDisasm.json 

LIBS += $$PWD/capstone/libcapstone.a

RESOURCES += \
    resource.qrc

TRANSLATIONS += \
    $$PWD/WingHexDisasm.ts
