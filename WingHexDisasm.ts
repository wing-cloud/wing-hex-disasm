<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AuthorDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">关于作者</translation>
    </message>
</context>
<context>
    <name>SponsorDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">赞助</translation>
    </message>
    <message>
        <source>Thanks for sponsor!</source>
        <translation type="vanished">感谢大家的赞助支持！</translation>
    </message>
</context>
<context>
    <name>WingHexDisasm</name>
    <message>
        <location filename="winghexdisasm.cpp" line="41"/>
        <source>Arch :</source>
        <translation>架构：</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="109"/>
        <source>Mode :</source>
        <translation>模式：</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="157"/>
        <source>DisasmWindow</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="159"/>
        <location filename="winghexdisasm.cpp" line="209"/>
        <source>WingHexDisasm</source>
        <translation>羽云反汇编器</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="163"/>
        <location filename="winghexdisasm.cpp" line="185"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="161"/>
        <location filename="winghexdisasm.cpp" line="183"/>
        <source>Disasm</source>
        <translation>反汇编</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="166"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="172"/>
        <source>Sponsor</source>
        <translation>赞助</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="218"/>
        <source>A small disassembly plugin for WingHexExplorer.</source>
        <oldsource>A small disasmbly plugin for WingHexExplorer.</oldsource>
        <translation>一个轻量的羽云十六进制编辑器的反汇编插件。</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="232"/>
        <source>Error: Failed capstone initialization</source>
        <translation>【错误】无法初始化 capstone 反汇编引擎</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="258"/>
        <source>Error: Failed to disassemble given op codes</source>
        <translation>【错误】无法对选中字节进行反汇编</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="267"/>
        <source>No Document Opened</source>
        <translation>没有任何打开的文档！</translation>
    </message>
    <message>
        <location filename="winghexdisasm.cpp" line="273"/>
        <source>No Selection Bytes</source>
        <translation>无选择字节！</translation>
    </message>
</context>
</TS>
