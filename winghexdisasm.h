#ifndef GENERICPLUGIN_H
#define GENERICPLUGIN_H

#include "../WingHexExplorer/WingHexExplorer/plugin/iwingplugin.h"
#include "capstone/capstone.h"
#include <QCheckBox>
#include <QComboBox>
#include <QTextBrowser>

class WingHexDisasm : public IWingPlugin {
  Q_OBJECT
#if QT_VERSION >= 0x050000
  Q_PLUGIN_METADATA(IID IWINGPLUGIN_INTERFACE_IID FILE "WingHexDisasm.json")
#endif // QT_VERSION >= 0x050000

  Q_INTERFACES(IWingPlugin)

public:
  WingHexDisasm(QObject *parent = nullptr);
  bool init(QList<WingPluginInfo> loadedplugin) override;
  ~WingHexDisasm() override;
  void unload() override;
  int sdkVersion() override;
  QMenu *registerMenu() override;
  QToolButton *registerToolButton() override;
  void
  registerDockWidget(QHash<QDockWidget *, Qt::DockWidgetArea> &rdw) override;
  const QString pluginName() override;
  const QString pluginAuthor() override;
  uint pluginVersion() override;
  const QString signature() override;
  const QString pluginComment() override;
  void plugin2MessagePipe(WingPluginMessage type, QList<QVariant> msg) override;

private:
  void disasm(QByteArray code);
  void on_disasm();
  void on_clear();
  void saveSetting();
  void loadSetting();

private:
  QMenu *menu;
  QToolButton *tbtn;
  QWidget *w;
  QDockWidget *dw;
  QTextBrowser *txtAsm;
  QComboBox *arch, *mode;

  QCheckBox *cbintel;
  QByteArray tmpbuf;

  cs_arch carch = cs_arch::CS_ARCH_X86;
  cs_mode cmode = cs_mode::CS_MODE_64;
};

#endif // GENERICPLUGIN_H
