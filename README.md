<h1 align="center">WingSummer.WingHexDisasm</h1>

<p align="center">
<img alt="WingHexPy" src="img/icon.png">
<p align="center">WingHexDisasm</p>
</p>

<p align="center">
<img alt="作者" src="https://img.shields.io/badge/Author-Wingsummer-green">
<img alt="开源协议" src="https://img.shields.io/badge/License-AGPL--3.0-red">
</p>

- 开源不易，给个 Star 或者 [捐助](#捐助) 吧

## WingHexDisasm

&emsp;&emsp;`WingHexDisasm`是一个羽云十六进制编辑器插件，它具有对给出字节进行反汇编的能力，基于`capstone`反汇编引擎框架。

### 协议

&emsp;&emsp;本插件仓库将采用`AGPL-3.0`协议，不得将该插件代码用于改协议之外的用途。

## 效果图

<p align="center">
<img alt="效果图" src="screenshot.png">
<p align="center">WingHexDisasm</p>
</p>

## 如何使用

&emsp;&emsp;将插件注册到软件的插件系统当中之后，只需在文档中用鼠标框选字节，执行反汇编命令即可。

## 注意事项

&emsp;&emsp;本插件仅支持`SDKVERSION >= 8`以上的“羽云十六进制编辑器”，也就是 1.4.8 及其以上的版本（该版本未被发布，请自行编译，预计 2022/8/25 发布该版本）。

## 捐助

**<p align="center">您的每一份支持都将是本项目推进的强大动力，十分感谢您的支持</p>**

<p align="center">

<img alt="支付宝" src="支付宝捐助.jpg" height=50% width=50%>
<p align="center">感谢支持</p>

</p>

<p align="center">
<img alt="微信" src="微信捐助.png" height=50% width=50%>
<p align="center">感谢支持</p>

</p>

## 有关仓库

* Gitea : https://code.gitlink.org.cn/wingsummer/WingHexDisasm
* Gitee : https://gitee.com/wing-cloud/wing-hex-disasm
