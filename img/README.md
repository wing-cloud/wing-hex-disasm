## WingHexDisasm

&emsp;&emsp;`WingHexDisasm`是一个羽云十六进制编辑器插件，它具有对给出字节进行反汇编的能力，基于`capstone`反汇编引擎框架。

### 协议

&emsp;&emsp;本插件仓库将采用`AGPL-3.0`协议，不得将该插件代码用于改协议之外的用途。

## 如何使用

&emsp;&emsp;将插件注册到软件的插件系统当中之后，只需在文档中用鼠标框选字节，执行反汇编命令即可。

## 有关仓库

* Gitea : https://code.gitlink.org.cn/wingsummer/WingHexDisasm
* Gitee : https://gitee.com/wing-cloud/wing-hex-disasm
